Source: pytest-fail-slow
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ileana Dumitrescu <ileanadumitrescu95@gmail.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-pytest <!nocheck>,
               pybuild-plugin-pyproject
Standards-Version: 4.6.1
Homepage: https://github.com/jwodder/pytest-fail-slow
Vcs-Git: https://salsa.debian.org/python-team/packages/pytest-fail-slow.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pytest-fail-slow
Rules-Requires-Root: no

Package: python3-pytest-fail-slow
Architecture: any
Multi-Arch: foreign
Depends: ${python3:Depends}, ${misc:Depends}
Description: Pytest plugin to fail tests that take too long to run
 A pytest plugin for making tests fail that take too long to run. It adds a
 --fail-slow DURATION command-line option to pytest that causes any & all
 otherwise-passing tests that run for longer than the given duration to be
 marked as failures, and it adds a @pytest.mark.fail_slow(DURATION) marker
 for making an individual test fail if it runs for longer than the given
 duration. If --fail-slow is given and a test has the @fail_slow() marker, the
 duration given by the marker takes precedence for that test.
